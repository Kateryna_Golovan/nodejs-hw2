const express = require('express');
const router = new express.Router();

const {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    changeCompletedFNoteByIdForUser,
    deleteNoteByIdForUser
} = require('../services/notesService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const offset = req.query.offset;
    const limit = req.query.limit;

    const notes = await getNotesByUserId(userId, offset, limit);

    res.json(notes);
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addNoteToUser(userId, req.body);

    res.json({message: "Note created successfully"});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const note = await getNoteByIdForUser(id, userId);

    if (!note) {
        throw new InvalidRequestError('No note with such id found!');
    }

    res.json(note);
}));


router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const { text } = req.body;

    await updateNoteByIdForUser(id, userId, text);

    res.json({message: "Note updated successfully"});
}));
  
router.patch('/:id', asyncWrapper(async (req, res) => {

    const {userId} = req.user;
    const {id} = req.params;

    await changeCompletedFNoteByIdForUser(id, userId);

    res.json({message: "Note updated successfully"});
}));
  
router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await deleteNoteByIdForUser(id, userId);

    res.json({message: "Note deleted successfully"});
}));

module.exports = {
    notesRouter: router
}