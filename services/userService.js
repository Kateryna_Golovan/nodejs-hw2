const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserInfo = async ( userId) => {
    const user = await User.findById(userId);
    return user;
}

const deleteUser = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

const changeUserPassword = async (userId, newPassword, oldPassword) => {
  const user = await User.findOne({_id: userId});
  const compared = await bcrypt.compare(oldPassword, user.password);
  
  if(!compared) {
    return false
  }
 
  await User.findOneAndUpdate({_id: userId}, {password: await  bcrypt.hash(newPassword, 10)});
  return true;
};


  /*
    if (!oldPassword) {
      return res.status(400).json({message: 'Please insert the password'});
    }
    if (!newPassword) {
      return res.status(400).json({message: 'Please insert the password'});
    }
  
    if (oldPassword === newPassword) {
      return res.status(400).json({message: 'Passwords can not be the same'});
    }
  
    try {
      const user = await User.findById(userId);
      if (!user) {
        return res.status(400).json({message: `User not found`});
      }
  
      if (!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({message: 'Wrong password'});
      }
  
      await User.findByIdAndUpdate(user._id, {
        password: await bcrypt.hash(newPassword, 10),
      });
  
      res.json({message: 'Success'});
    } catch (error) {
      res.status(500).json({message: 'Server error'});
    }
  };
  */
  

  
  module.exports = {
    getUserInfo,
    deleteUser,
    changeUserPassword    
  };