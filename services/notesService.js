const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, offset = 0, limit = 0) => {
    const notes = await Note.find({userId})
    .skip(Number(offset))
    .limit(Number(limit));
    return { 
        offset,
        limit,
        count: notes.length,
        notes: notes
    };
}

const getNoteByIdForUser = async (id, userId) => {
    const note = await Note.findOne({_id: id, userId});
    return note;
  }
  

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}

const updateNoteByIdForUser = async (id, userId, dattexta) => {
   return await Note.findOneAndUpdate({_id: id, userId}, { $set: {text}});
}

const changeCompletedFNoteByIdForUser = async (noteId, userId, data) => {
    const completed = note.completed ? false : true;
    await Note.findOneAndUpdate({_id: id, userId}, {completed});
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    changeCompletedFNoteByIdForUser,
    deleteNoteByIdForUser
};