const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    const {
        authorization
    } = req.headers;
  
    if (!authorization) {
        return res.status(401).json({message: 'Please, provide "authorization" header'});
    }
  
    const [, jwt_token] = authorization.split(' ');
  
    if (!jwt_token) {
        return res.status(401).json({message: 'Please, include token to request'});
    }
  
    try {
      const tokenPayload = jwt.verify(jwt_token, 'secret');
      req.user = {
        userId: tokenPayload._id,
        email: tokenPayload.email,
        role: tokenPayload.role,
      };
      next();
    } catch (error) {
      res.status(401).json({message: error.message});
    }
  };

module.exports = {
    authMiddleware
}